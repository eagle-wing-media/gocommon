package osutils


//OSUtilsDefine is an interface containing methods for interacting with the Operating System
type OSUtilsDefine interface {
	GetEnvVar(name string) string
}