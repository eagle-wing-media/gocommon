package osutils

import "os"

//--------------------------------
// LIVE CLASSES
//--------------------------------

//OSUtils is a struct containing Operating-System related methods and data
type OSUtils struct {
}

//GetEnvVar returns the specified environment variable data, or an empty string if it does not exist
func (o *OSUtils) GetEnvVar(name string) string {
	result := ""

	if len(name) > 0 {
		result = os.Getenv(name)
	}

	return result
}

//--------------------------------
// MOCK CLASSES
//--------------------------------

//MockOSUtils mocks the OSUtils
type MockOSUtils struct {
	GetEnvVarCallCount int8
	GetEnvVarResults   map[string]string
}

//GetEnvVar mocks OSUtils.GetEnvVar, result set in GetEnvVarResults map, increments GetEnvVarCallCount on each call
func (o *MockOSUtils) GetEnvVar(name string) string {
	o.GetEnvVarCallCount++
	return o.GetEnvVarResults[name]
}
