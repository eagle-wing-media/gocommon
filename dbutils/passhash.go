package dbutils

import (
	"crypto/sha512"
	"encoding/hex"
)

/*HashPassword performs a secure hashing process against the given raw password,
returns the hashed password or a blank string on error*/
func HashPassword(input string) string {
	result := ""

	if len(input) > 0 {
		if inputBytes := []byte(input); len(inputBytes) > 0 {
			rawOutput := sha512.Sum512(inputBytes)
			result = hex.EncodeToString(rawOutput[:64])
		}
	}

	return result
}
