package dbutils

import (
	"database/sql"
	"errors"
	"time"
)

//waitTimeSeconds is how long 'IsResponding' uses as a timeout
const waitTimeSeconds = 30

//IsResponding returns whether a database is responding to the given *sql.DB
func IsResponding(db *sql.DB) (bool, error) {
	var err error
	seconds := 0

	if db != nil {
		for seconds < waitTimeSeconds {
			err = db.Ping()

			if err != nil {
				seconds++
				time.Sleep(1 * time.Second)
			} else {
				return true, nil
			}
		}
	} else {
		err = errors.New("Invalid Pointer To 'dbutils.IsResponding'")
	}

	return false, err
}
