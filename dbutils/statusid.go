package dbutils

//Database Status's
const (
	StatusError    = -1
	StatusDisabled = 0
	StatusEnabled  = 1
	StatusDeleted  = 2
)
