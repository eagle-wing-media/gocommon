package dbutils

//ID's for applications
const (
	AppIDGatekeeper = 1
)

//DefaultLicenceLength is the default time for which a licence is held, between an account & application. Figure is in Days
const DefaultLicenceLength float64 = 30
