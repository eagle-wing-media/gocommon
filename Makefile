
all:
	@$(MAKE) test success || $(MAKE) failure

clean:
	if [ -f ./coverage.html ]; then rm ./coverage.html; fi;
	if [ -f ./coverage.out ]; then rm ./coverage.out; fi;

test:
	@$(MAKE) clean
	go test ./... -coverprofile=coverage.out
	go tool cover -html=coverage.out -o coverage.html

success:
	printf "\n\e[1;32mBuild Successful\e[0m\n"

failure:
	printf "\n\e[1;31mBuild Failure\e[0m\n" 
	exit 1