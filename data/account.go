package data

//Account represents the raw data held for an account in the database
type Account struct {
	ID       int
	Name     string
	StatusID int
}
