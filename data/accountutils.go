package data

//AccountToMap takes an Account object and returns a map ready for JSON marshalling
func AccountToMap(acc *Account) map[string]interface{} {
	if acc != nil {
		result := make(map[string]interface{})
		result["id"] = acc.ID
		result["name"] = acc.Name
		result["status_id"] = acc.StatusID
		return result
	}

	return nil
}
