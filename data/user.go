package data

//User represents a user within the database
type User struct {
	UserID            int
	Username          string
	RawPassword       string
	HashPassword      string
	EmailAddress      string
	AccountID         int
	UserStatusID      int
	PermissionLevelID int
	Verified          int
}
