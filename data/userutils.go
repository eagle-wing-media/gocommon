package data

//UserToMap takes a User object and returns a map version of it, or nil if there was an error
func UserToMap(user *User, returnPassword, returnEmail bool) map[string]interface{} {
	var result map[string]interface{}

	if user != nil {
		result = make(map[string]interface{})
		result["user_id"] = user.UserID
		result["username"] = user.Username
		result["user_status_id"] = user.UserStatusID
		result["account_id"] = user.AccountID
		result["permission_level_id"] = user.PermissionLevelID
		result["verified"] = user.Verified

		if returnPassword {
			result["hash_password"] = user.HashPassword
		}

		if returnEmail {
			result["email_address"] = user.EmailAddress
		}
	}

	return result
}
