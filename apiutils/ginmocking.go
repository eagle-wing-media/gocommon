package apiutils

import (
	"bytes"
	"encoding/json"
	"net/http/httptest"

	"github.com/gin-gonic/gin"
)

//MockGinContextFactory creates a *gin.Context object for testing
func MockGinContextFactory(inputURL string, method string, reqBody map[string]interface{},
	params map[string]string, header map[string]string) *gin.Context {

	paramOut := make(gin.Params, 0)
	for key, value := range params {
		tempParam := gin.Param{Key: key, Value: value}
		paramOut = append(paramOut, tempParam)
	}

	if bodyStr, err := json.Marshal(reqBody); err == nil {
		request := httptest.NewRequest(method, inputURL, bytes.NewBuffer(bodyStr))
		for key, value := range header {
			request.Header.Add(key, value)
		}

		context, _ := gin.CreateTestContext(httptest.NewRecorder())
		context.Params = paramOut
		context.Request = request
		return context
	}

	return nil
}
