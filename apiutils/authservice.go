package apiutils

import (
	"errors"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

//AuthServiceMiddleware is a struct representing the AuthService within Gin Middleware
type AuthServiceMiddleware struct {
	EndpointAddress string
}

//Authenticate takes a gin.Context, extracts the bearer token and authenticates it with the AuthService
func (as *AuthServiceMiddleware) Authenticate(c *gin.Context) {
	var err error
	var resp *http.Response
	address := "http://" + as.EndpointAddress + "/api/auth/helloservice"

	if resp, err = http.Get(address); err == nil {
		defer resp.Body.Close()
		if resp.StatusCode == http.StatusOK {
			c.Next()
		} else if resp.StatusCode == http.StatusUnauthorized {
			c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"Error": "Token Not Authorized"})
		} else {
			err = errors.New("Unhandled HTTP Status Returned From AuthService: " + strconv.Itoa(resp.StatusCode))
		}
	}

	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"Error": err.Error()})
	}
}
