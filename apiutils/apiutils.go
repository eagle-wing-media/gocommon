package apiutils

import (
	"errors"
	"net/http"
	"strconv"
)

//CreateBoundPort returns a string of :port, configured from constructor
func CreateBoundPort(port int) (string, error) {
	if port >= 1 && port <= 65535 {
		boundPort := ":" + strconv.Itoa(port)

		if len(boundPort) == 0 {
			return "", errors.New("Error Creating boundPort")
		}

		return boundPort, nil
	}

	return "", errors.New("Invalid Port")
}

//GenerateBadReqError returns a JSON response formatted as our api standard 400 response
func GenerateBadReqError(userError string) (int, map[string]interface{}) {
	resp := make(map[string]interface{})
	resp["error"] = userError
	return http.StatusBadRequest, resp
}

//GenerateServerError returns a JSON response formatted as our api standard 500 response
func GenerateServerError(errorTitle, errorText string) (int, map[string]interface{}) {
	resp := make(map[string]interface{})
	resp["error"] = errorTitle
	resp["error_text"] = errorText
	return http.StatusInternalServerError, resp
}

//GenerateConflictError returns a JSON response formatted as our api standard 409 response
func GenerateConflictError(userError string) (int, map[string]interface{}) {
	resp := make(map[string]interface{})
	resp["error"] = userError
	return http.StatusConflict, resp
}
