package emailserviceclient

import (
	"errors"
)

//EmailServiceClient represents a client for the email service
type EmailServiceClient struct {
	debug bool
}

//SendEmail contacts the email service and sends the requested email
func (ec *EmailServiceClient) SendEmail(fromAddress, toAddress, content string) (bool, error) {
	return false, errors.New("Not Implemented")
}
