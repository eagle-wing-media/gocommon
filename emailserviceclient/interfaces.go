package emailserviceclient

//Define represents a definition for an email service client
type Define interface {
	SendEmail(fromAddress, toAddress, content string) (bool, error)
}
